//webpack.config.js

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
var path = require("path");

module.exports = {
    entry: ['./src/js/main.js', './src/style/stylesheet.scss'],
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude:/node_modules/,
                loader: 'babel-loader',
            },
            {
                test: /\.scss$/,
                use: [
                MiniCssExtractPlugin.loader,
                  'css-loader',
                  'sass-loader',
                 
                ],
                
              },
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
          filename: "[name].css",
          chunkFilename: "[id].css"
        })
    ],
    devServer: {
        port: 3000
    }
};