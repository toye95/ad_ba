const selectById = sel => {
    return document.getElementById(sel);
  };
  
const selectByClassName = sel => {
    return document.getElementsByClassName(sel);
  };
  
const toggleTabs = (event, sel) => {
    let i, tabcontent, tablinks;
    tabcontent = selectByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = selectByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
   
    document.querySelector(`.profile-details #${sel}`).style.display = "block";
    event.currentTarget.className += " active";
  };
  
export const initTabEventListeners = () => {

  let tabContents = document.querySelectorAll('.tabcontent');
  let tabLinks = document.querySelectorAll('.tablinks');

  for (let i = 0; i < tabLinks.length; i++) {
    tabLinks[i].addEventListener('click', (event) => {
      toggleTabs(event, event.target.id);
    })
  }

  }
  
  
  
  