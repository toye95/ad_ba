import { initTabEventListeners } from './tabs-component';
import { initPopover } from './popover-component';
import { initMobileEdit } from './mobile-edit-component';

//Initialize tab events.
initTabEventListeners();

//Initialize popover.
initPopover();

//Mobile edit
initMobileEdit();
