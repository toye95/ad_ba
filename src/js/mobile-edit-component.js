let items = document.querySelectorAll('.pill span');
let inputs = document.querySelectorAll('.edit-form-mobile .group input');

export const initMobileEdit = () => {
    document.getElementById('edit-btn').addEventListener('click', (event) => {

        inputs[0].value = items[0].innerHTML;
        inputs[1].value = items[1].innerHTML;
        inputs[2].value = items[2].innerHTML;
        inputs[3].value = items[3].innerHTML;
    
       openEdit();
    })
    
    document.getElementById('edit-save').addEventListener('click', (event) => {
        items[0].innerHTML = inputs[0].value;
        items[1].innerHTML = inputs[1].value;
        items[2].innerHTML = inputs[2].value;
        items[3].innerHTML = inputs[3].value;
    
      closeEdit();
    })
}


document.getElementById('edit-cancel').addEventListener('click', (event) => {
    closeEdit();
})

const closeEdit = () => {
    document.querySelector('.details-container').style.display = 'block';
    document.querySelector('.edit-form-mobile').style.display = 'none';
}

const openEdit = () => {
    document.querySelector('.details-container').style.display = 'none';
    document.querySelector('.edit-form-mobile').style.display = 'block';
}
