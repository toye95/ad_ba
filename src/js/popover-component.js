
let popovers = document.querySelectorAll('.details');
let popoverTriggers = document.querySelectorAll('.popover-trigger');
let saveButtons = document.querySelectorAll('.submit-btn');
let cancelButtons = document.querySelectorAll('.cancel-btn');

let parent = '';

export const initPopover = () => {
    for (let i = 0; i < popoverTriggers.length; i++) {
       
        popoverTriggers[i].addEventListener('click', function(event) {
            closeAllOthers(this.parentElement);
            parent = this.parentElement;
            this.parentElement.querySelector('.popover .popover-menu').classList.remove('hidden');
            this.parentElement.querySelector('div div div input').value = this.parentElement.querySelector('span span').innerHTML;      
            this.parentElement.querySelector('div div div input').focus();
        });
    }
    
    for (let i = 0; i < saveButtons.length; i++) {
        saveButtons[i].addEventListener('click', (event) => {
            parent.querySelector('span span').innerHTML = parent.querySelector('div div div input').value;
            close(parent);
        })
    }
    
    for (let i = 0; i < cancelButtons.length; i++) {
        cancelButtons[i].addEventListener('click', (event) => {
            close(parent);
        })
    }
}

const closeAllOthers = (ignore) =>  {
	for (let i = 0; i < popovers.length; i++) {
		if ( popovers[i] !== ignore) {
			popovers[i].querySelector('.popover .popover-menu').classList.add('hidden');	
		}
	}
}

const close = (parent) => {
    parent.querySelector('.popover .popover-menu').classList.add('hidden');
}



